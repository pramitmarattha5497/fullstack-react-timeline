import React from 'react'
import {Events,UrlButton,ImageEvent} from '@merc/react-timeline';
const Allproject = () => {
    return (
        <>
        <Events>

{/* <---------------------------------------------------------------------------------------------------------------------------------> */}
        <ImageEvent
          date="Sep 25 , 2020"
          text="1) Mini Code Editor"
          src="https://user-images.githubusercontent.com/37651620/93988236-32647980-fda8-11ea-9052-3238ea4a42b2.png"
          alt="Mini Code Editor"
        >
        <div>
        <UrlButton href="https://mini-code-editor.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/code-editor-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>
        
{/* <---------------------------------------------------------------------------------------------------------------------------------> */}

        <ImageEvent
          date="Sep 26 , 2020"
          text="2) Community Chat App with react and firebase"
          src="https://user-images.githubusercontent.com/37651620/94143516-a7f74500-fe8f-11ea-9105-188e31f1d62a.png"
          alt="Community Chat App with react and firebase"
        >
        <div>
        <UrlButton href="https://community-chat-app.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/community-chat-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>

{/* <---------------------------------------------------------------------------------------------------------------------------------> */}


        <ImageEvent
          date="Sep 28 , 2020"
          text="3) Awesome Dark Mode App"
          src="https://user-images.githubusercontent.com/37651620/94997593-f2816b80-05cb-11eb-8b99-8fa1fa71fada.png"
          alt="Awesome Dark Mode"
        >
        <div>
        <UrlButton href="https://dark-mode-switcher.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/dark-mode-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>





{/* <---------------------------------------------------------------------------------------------------------------------------------> */}


        <ImageEvent
          date="Sep 29 , 2020"
          text="4) Notetaking notebook app"
          src="https://user-images.githubusercontent.com/37651620/94556131-54fd0380-027c-11eb-9926-c55a3130bb36.png"
          alt="Notetaking notebook app"
        >
        <div>
        <UrlButton href="https://notebook-app.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/notebook-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>




{/* <---------------------------------------------------------------------------------------------------------------------------------> */}



        <ImageEvent
          date="Sep 30 , 2020"
          text="5) Conference Schedule App"
          src="https://user-images.githubusercontent.com/37651620/94646070-2844fc80-030d-11eb-8fe5-cdc881f88d8c.png"
          alt="Conference Schedule App"
        >
        <div>
        <UrlButton href="https://conference-schedule-app.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/conference-schedule-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>



{/* <---------------------------------------------------------------------------------------------------------------------------------> */}


        <ImageEvent
          date="Oct 2, 2020"
          text="6) Tetris React Game"
          src="https://user-images.githubusercontent.com/37651620/94886472-fd84b080-0492-11eb-9668-db1f2a612b8c.png"
          alt="Tetris React Game"
        >
        <div>
        <UrlButton href="https://tetramino-tetris.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/tetris-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>



{/* <---------------------------------------------------------------------------------------------------------------------------------> */}

        <ImageEvent
          date="Oct 3, 2020"
          text="7) Bookmark App"
          src="https://user-images.githubusercontent.com/37651620/94985851-ab6a8a80-0579-11eb-8f51-fa35d314c588.png"
          alt="Bookmark App"
        >
        <div>
        <UrlButton href="https://book-mark-react.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/bookmark-manager-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>



{/* <---------------------------------------------------------------------------------------------------------------------------------> */}

        <ImageEvent
          date="Oct 4, 2020"
          text="8) Fetch Movies"
          src="https://user-images.githubusercontent.com/37651620/95007974-4b391e80-0635-11eb-91fb-040e6b55bfcf.png"
          alt="Fetch Movies"
        >
        <div>
        <UrlButton href="https://fetch-movies.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/fetch-movies-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>



{/* <---------------------------------------------------------------------------------------------------------------------------------> */}
        <ImageEvent
          date="Oct 4, 2020"
          text="9) Grocery List Manager"
          src="https://user-images.githubusercontent.com/37651620/95011170-8136cc80-064e-11eb-9b4b-8200b1fdf290.png"
          alt="Grocery List Manager"
        >
        <div>
        <UrlButton href="https://grocery-list-manager.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/grocery-list-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>


{/* <---------------------------------------------------------------------------------------------------------------------------------> */}

        <ImageEvent
          date="Oct 5, 2020"
          text="10) Pokemon Info (pokedex) App."
          src="https://user-images.githubusercontent.com/37651620/95025734-aefe2e80-06ab-11eb-9915-ffad4e7014ed.png"
          alt="Pokemon Info (pokedex) App"
        >
        <div>
        <UrlButton href="https://pokemon-info-app.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/pokemon-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>

{/* <---------------------------------------------------------------------------------------------------------------------------------> */}


<ImageEvent
          date="Oct 12, 2020"
          text="11) Budget Manager and calculator."
          src="https://user-images.githubusercontent.com/37651620/96116885-e91bcb80-0f08-11eb-8653-b0dbb082f68c.png"
          alt="Budget Manager and calculator"
        >
        <div>
        <UrlButton href="https://budget-manager-react.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/budget-manager-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>

{/* <---------------------------------------------------------------------------------------------------------------------------------> */}


<ImageEvent
          date="Oct 29, 2020"
          text="12) Email Newsletter app"
          src="https://user-images.githubusercontent.com/37651620/97455969-80007300-1960-11eb-8be3-876c8f875c1e.png"
          alt="Newsletter web app"
        >
        <div>
        <UrlButton href="https://newsletter-react-app.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/newsletter-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>

{/* <---------------------------------------------------------------------------------------------------------------------------------> */}


<ImageEvent
          date="Nov 10, 2020"
          text="13) Live Music Player React."
          src="https://user-images.githubusercontent.com/37651620/98583627-c6ed5180-22ec-11eb-999d-c6921f61ac93.png"
          alt=""
        >
        <div>
        <UrlButton href="https://music-player-react.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/music-player-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>
        
{/* <---------------------------------------------------------------------------------------------------------------------------------> */}


<ImageEvent
          date="Nov 11, 2020"
          text="14) Space Invader Game."
          src="https://user-images.githubusercontent.com/37651620/98832957-911fa880-2465-11eb-8249-f1507f39cd18.gif"
          alt="Space Invader Game"
        >
        <div>
        <UrlButton href="https://space-invader-react.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/space-invader-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>


{/* <---------------------------------------------------------------------------------------------------------------------------------> */}


<ImageEvent
          date="Nov 20, 2020"
          text="15) Virtual Piano (React Musical Instrument)."
          src="https://user-images.githubusercontent.com/37651620/100707288-8ad07c80-33d2-11eb-937c-8b38bb9d8ca1.png"
          alt="Virtual Piano"
        >
        <div>
        <UrlButton href="https://virtual-piano-instrument.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/musical-instrument-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>

        
{/* <---------------------------------------------------------------------------------------------------------------------------------> */}


<ImageEvent
          date="Nov 25, 2020"
          text="16) React moodboard (drag and drop images)."
          src="https://user-images.githubusercontent.com/37651620/101074345-bb7a0700-35c8-11eb-9bde-c9d9b12c63ba.gif"
          alt="MoodBoard"
        >
        <div>
        <UrlButton href="https://moodboard-dnd.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/mood-board-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>

        
{/* <---------------------------------------------------------------------------------------------------------------------------------> */}


<ImageEvent
          date="Nov 27, 2020"
          text="17) React Admin Dashboard."
          src="https://user-images.githubusercontent.com/37651620/101245506-7aebcc00-3735-11eb-9bc4-94dab2475dc1.gif"
          alt="React Admin Dashboard"
        >
        <div>
        <UrlButton href="https://simple-admin-dashboard.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/website-admin-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>

        
{/* <---------------------------------------------------------------------------------------------------------------------------------> */}


<ImageEvent
          date="Dec 4, 2020"
          text="18) Wiki Search."
          src="https://user-images.githubusercontent.com/37651620/101260703-b6988d00-3759-11eb-83dd-1b3b9242f658.gif"
          alt="Wiki Search"
        >
        <div>
        <UrlButton href="https://react-wiki-search.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/fan-wiki-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>

        
{/* <---------------------------------------------------------------------------------------------------------------------------------> */}


<ImageEvent
          date="Dec 6, 2020"
          text="19) Simple Chess Game."
          src="https://user-images.githubusercontent.com/37651620/101364822-1b272980-38cb-11eb-8854-ecb490203bc6.png"
          alt="Simple Chess Game"
        >
        <div>
        <UrlButton href="https://simple-chess-react.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/chess-game-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>

        
{/* <---------------------------------------------------------------------------------------------------------------------------------> */}


<ImageEvent
          date="Dec 9, 2020"
          text="20) Tesla Mileage Evaluator."
          src="https://user-images.githubusercontent.com/37651620/101777840-22dd0d00-3b1b-11eb-8686-a3db95daae8c.gif"
          alt="Tesla Mileage Evaluator"
        >
        <div>
        <UrlButton href="https://tesla-mileage.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/tesla-tracker-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>

{/* <---------------------------------------------------------------------------------------------------------------------------------> */}


<ImageEvent
          date="Dec 15, 2020"
          text="21) Road Trip Map."
          src="https://user-images.githubusercontent.com/37651620/102373927-d301b880-3fe8-11eb-85b7-2628268a0ccd.png"
          alt="Road Trip Map Locater"
        >
        <div>
        <UrlButton href="https://road-trip-map.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Fullstack-projects-frontend-with-react-and-backend-with-various-stacks/tree/master/road-trip-map-react-web-app-project">Source Code</UrlButton>
        </div>
        </ImageEvent>


{/* <---------------------------------------------------------------------------------------------------------------------------------> */}

<ImageEvent
          date="Dec 16, 2020"
          text="22) Kitchen App ."
          src="https://user-images.githubusercontent.com/37651620/92781623-03233500-f3c4-11ea-9462-2d0e60246a50.gif"
          alt="Kitchen App"
        >
        <div>
        <UrlButton href="https://completekitchenapp.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/React-practice-app-project-in-depth-guide">Source Code</UrlButton>
        </div>
        </ImageEvent>

        {/* <---------------------------------------------------------------------------------------------------------------------------------> */}

<ImageEvent
          date="Dec 17, 2020"
          text="23) Chat with World ."
          src="https://user-images.githubusercontent.com/37651620/87466868-c0004c00-c636-11ea-9cb3-5e807bf24791.gif"
          alt="Chat with World"
        >
        <div>
        <UrlButton href="https://shareyourvision.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Chat-With-World-WebApp">Source Code</UrlButton>
        </div>
        </ImageEvent>
 {/* <---------------------------------------------------------------------------------------------------------------------------------> */}

<ImageEvent
          date="Dec 17, 2020"
          text="24) Search Job Hub ."
          src="https://user-images.githubusercontent.com/37651620/88305716-8b248100-cd29-11ea-9299-d759d3c4c507.gif"
          alt="Search Job Hub"
        >
        <div>
        <UrlButton href="https://searchjobhub.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/SearchJobHub-Easy-JobSearch-WebApp">Source Code</UrlButton>
        </div>
        </ImageEvent>

   {/* <---------------------------------------------------------------------------------------------------------------------------------> */}

<ImageEvent
          date="Dec 18, 2020"
          text="23) Pdf Exporter ."
          src="https://user-images.githubusercontent.com/37651620/91534402-33cd8e00-e931-11ea-8e2c-1d1de0e8ac1b.png"
          alt="Pdf Exporter"
        >
        <div>
        <UrlButton href="https://export-as-pdf.netlify.app/">Visit Site <span role="img" aria-label="celebrate" >🎉</span></UrlButton>
            <UrlButton href="https://github.com/pramit-marattha/Exporting-As-Pdf-React-App">Source Code</UrlButton>
        </div>
        </ImageEvent>


    </Events>
        </>
    )
}

export default Allproject
